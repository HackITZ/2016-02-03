% HackITZ 32C3, 2016-02-03
% Max Albrecht; Beat Rohrer

# HackITZ 32C3

# Inhalt

- Photos/Intro
- Talks
    - Red Star OS
    - „Dieselgate“
    - Diffie-Hellman/Logjam
    - Email Security
- Minis
    - Let's Encrypt
    - Netzpolitik in der Schweiz
    - rad1o++
    - Tor Detection
- Links

# Photos

- grösste "Hacker"-Konferenz in Europa
- seit 1984
- 32C3 27.–30.12.'15, Congress Centrum Hamburg
- diesmal fast voll, kein größerer Ort in Zentraleuropa

# Vorträge

#

## Red Star OS

<https://media.ccc.de/v/32c3-7174-lifting_the_fog_on_red_star_os>

[![](https://lab.dsst.io/32c3-slides/slides/7174/13.jpg)](https://lab.dsst.io/32c3-slides/slides/7174/13.jpg)

## 

- gehärtete kommunistische Linux-Distro
- `libstasi`
- Wissen basiert nur auf geleaktem Install-Image
- s.a. Cory Doctorow: "The Coming War on General Purpose Computation"
    - Talk: <https://www.youtube.com/watch?v=HUEvRyemKSg>
    - Text: <http://boingboing.net/2012/01/10/lockdown.html>


## gehärtetes kommunistisches Linux

- "hardened" Linux mit sinnvollen(!) Sicherheits-Features
    - SELinux, ALSR, Kernelmodule, Watchdog-Prozesse
- ähnlich ChromeOS, OLPC; gute Localization und Defaults
- "just works", teilweise besser als Apple
- aber Apple kann TPM in den Chip giessen
- Bsp. Kernelmodul: `root`-Rechte einschränken
    - unkillbare Prozesse
    - unveränderbare Dateien/Programme
- Watchdog: Zus. Integritätscheck -> Reboot-Loop
- "Trusted Computing Platform" für eigenes Land
- Enduser hat eine "getroot" App wie OLPC, Android(?)

## `libstasi`

- Totale Kontolle von *Inhalten* (Medien-Dateien)
- **Tracker**: "Fingerprints" in jeder Datei auf jeden gmounteten Volume
- **"Virenscanner"**: Erkennt und löscht "verbotene" Dateien
    - wahrscheinlich petzt es auch
- Software Updates zentral, kann beliebige Hintertüren nachinstallieren

#

## The exhaust emissions scandal („Dieselgate“)

<https://media.ccc.de/v/32c3-7331-the_exhaust_emissions_scandal_dieselgate>

## Bescheissen gehört dazu

[![](https://lab.dsst.io/32c3-slides/slides/7331/20.jpg)](https://lab.dsst.io/32c3-slides/slides/7331/20.jpg)

## Katalysator I

[![](https://lab.dsst.io/32c3-slides/slides/7331/66.jpg)](https://lab.dsst.io/32c3-slides/slides/7331/66.jpg)

## Katalysator II

[![](https://lab.dsst.io/32c3-slides/slides/7331/69.jpg)](https://lab.dsst.io/32c3-slides/slides/7331/69.jpg)

## Main Model

[![](https://lab.dsst.io/32c3-slides/slides/7331/74.jpg)](https://lab.dsst.io/32c3-slides/slides/7331/74.jpg)

## Alternative Model

[![](https://lab.dsst.io/32c3-slides/slides/7331/78.jpg)](https://lab.dsst.io/32c3-slides/slides/7331/78.jpg)

## Model Selection

[![](https://lab.dsst.io/32c3-slides/slides/7331/89.jpg)](https://lab.dsst.io/32c3-slides/slides/7331/89.jpg)

## Test Protocol

[![](https://lab.dsst.io/32c3-slides/slides/7331/19.jpg)](https://lab.dsst.io/32c3-slides/slides/7331/19.jpg)

## Model Selection Plotted

[![](https://lab.dsst.io/32c3-slides/slides/7331/92.jpg)](https://lab.dsst.io/32c3-slides/slides/7331/92.jpg)

## Test Drive Plotted

[![](https://lab.dsst.io/32c3-slides/slides/7331/96.jpg)](https://lab.dsst.io/32c3-slides/slides/7331/96.jpg)

# 

## Logjam: Diffie-Hellman, discrete logs, the NSA, and you

<https://media.ccc.de/v/32c3-7288-logjam_diffie-hellman_discrete_logs_the_nsa_and_you>

## Textbook Diffie-Hellman

[![](https://lab.dsst.io/32c3-slides/slides/7288/16.jpg)](https://lab.dsst.io/32c3-slides/slides/7288/16.jpg)

## Breaking DH

[![](https://lab.dsst.io/32c3-slides/slides/7288/12.jpg)](https://lab.dsst.io/32c3-slides/slides/7288/12.jpg)

## Precomputation

[![](https://lab.dsst.io/32c3-slides/slides/7288/28.jpg)](https://lab.dsst.io/32c3-slides/slides/7288/28.jpg)

## Logjam

[![](https://lab.dsst.io/32c3-slides/slides/7288/43.jpg)](https://lab.dsst.io/32c3-slides/slides/7288/43.jpg)

# 

## Neither Snow Nor Rain Nor MITM… The State of Email Security in 2015

<https://media.ccc.de/v/32c3-7255-neither_snow_nor_rain_nor_mitm_the_state_of_email_security_in_2015>

## Transit Encryption Overview

[![](https://lab.dsst.io/32c3-slides/slides/7255/6.jpg)](https://lab.dsst.io/32c3-slides/slides/7255/6.jpg)

## Verbreitung Gmail STARTTLS

[![](https://lab.dsst.io/32c3-slides/slides/7255/23.jpg)](https://lab.dsst.io/32c3-slides/slides/7255/23.jpg)

## Certificate Problem

[![](https://lab.dsst.io/32c3-slides/slides/7255/17.jpg)](https://lab.dsst.io/32c3-slides/slides/7255/17.jpg)

## Stripping STARTTLS

[![](https://lab.dsst.io/32c3-slides/slides/7255/33.jpg)](https://lab.dsst.io/32c3-slides/slides/7255/33.jpg)

## Stripping going on

[![](https://lab.dsst.io/32c3-slides/slides/7255/34.jpg)](https://lab.dsst.io/32c3-slides/slides/7255/34.jpg)

## DNS spoofing

[![](https://lab.dsst.io/32c3-slides/slides/7255/39.jpg)](https://lab.dsst.io/32c3-slides/slides/7255/39.jpg)

## Improvements

[![](https://lab.dsst.io/32c3-slides/slides/7255/41.jpg)](https://lab.dsst.io/32c3-slides/slides/7255/41.jpg)

## Problems Sender Policy Framework (SPF)

*Welche IPs dürfen für eine Domains Mails verschicken*

- Mail Forwards
- Mailing Lists

## DomainKeys Identified Mail (DKIM)

*Mails signieren*

- man kann im DNS nicht nachschauen, ob alles gesigned sein soll
- Signatur kann entfernt werden

## Domain-based Message Authentication, Reporting and Conformance (DMARC)

- Mail kann legal verändert werden, z.B. Mailing Lists

## Verbreitung

[![](https://lab.dsst.io/32c3-slides/slides/7255/49.jpg)](https://lab.dsst.io/32c3-slides/slides/7255/49.jpg)

## Possible Solutions

[![](https://lab.dsst.io/32c3-slides/slides/7255/52.jpg)](https://lab.dsst.io/32c3-slides/slides/7255/52.jpg)

# Minis

# Let's Encrypt

<https://media.ccc.de/v/32c3-7528-let_s_encrypt_--_what_launching_a_free_ca_looks_like>

- Let's encrypt ist gestartet! (Dezember 2015 öffentlich)
- \>200.000 Zertifikate, ~4% insg. (Platz 5); zu 85% erstes Zert. für diese Domain!
- Client könnte besser sein, aber Quellcode und Prokoll ist offen…
- aktuell super für mit hostname=domainname (`myblog.example.com`)
- Koop. mit Hostingprovidern und CDNs, kostenloses SSL für Endkunden
    - 'litmus test': wer es jetzt schon hat 👍, wer es in 1-2 jahren nicht hat 👎
- in Zukunft: DNS challenge; dann auch super für `mycdn.example.com`
- Bald als Root-CA in Browsern/OS, aber Audits dauern…
- Aber: Unterstützung für *alle Clients* **sofort** (Cross-Sign durch bestehende CA)


#

## Netzpolitik in der Schweiz

<https://media.ccc.de/v/32c3-7205-netzpolitik_in_der_schweiz>

- Zusammenfassung der netzpolitischen Themen in CH in letzter Zeit
- **<https://nachrichtendienstgesetz.ch>**
- **<https://stopbuepf.ch>**

#

## rad1o++

![](https://lab.dsst.io/32c3-slides/slides/7153/0.jpg)

##

- Badge des CCC Camp 2015 war ein SDR-Devboard!
- Intro Talk: <https://media.ccc.de/v/camp2015-6884-the_rad1o>
- Updates: <https://media.ccc.de/v/32c3-7153-rad1o>
- HackRF! <http://greatscottgadgets.com/hackrf/>

# 

## How the Great Firewall discovers hidden circumvention servers
<https://media.ccc.de/v/32c3-7196-how_the_great_firewall_discovers_hidden_circumvention_servers>

## Tor Detection

[![](https://lab.dsst.io/32c3-slides/slides/7196/20.jpg)](https://lab.dsst.io/32c3-slides/slides/7196/20.jpg)

- verschiedene Protokolle versuchen bereits Tor-Traffic zu verstecken
- mit DPI kann man nicht erkennen, ob Traffic zum Tor-Netzwerk gehört

## Tor Probing

[![](https://lab.dsst.io/32c3-slides/slides/7196/24.jpg)](https://lab.dsst.io/32c3-slides/slides/7196/24.jpg)

- bei fraglichem Traffic nimmt The Great Firewall random chinesiche IPs und versucht eine Tor-Verbindung aufzubauen
- neues Tor-Protokoll ist in Arbeit, das mit einem Shared Secret funktioniert

# Links

- Slides: <https://gitlab.zhdk.ch/HackITZ/2016-02-03/blob/master/README.md>
- alle Videos: <https://media.ccc.de/c/32c3>