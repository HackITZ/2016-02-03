Liebe Kolleginnen und Kollegen

Wie versprochen findet ihr hier die Links zu unserem 32C3-HackITZ:

Slides:
https://gitlab.zhdk.ch/HackITZ/2016-02-03/blob/master/README.md

Videos zu den von uns vorgestellten Talks (mit Synchronisation und Untertitel):

- Red Star OS
  https://media.ccc.de/v/32c3-7174-lifting_the_fog_on_red_star_os>
- The exhaust emissions scandal („Dieselgate“)
  https://media.ccc.de/v/32c3-7331-the_exhaust_emissions_scandal_dieselgate
- Logjam: Diffie-Hellman, discrete logs, the NSA, and you
  https://media.ccc.de/v/32c3-7288-logjam_diffie-hellman_discrete_logs_the_nsa_and_you
- Neither Snow Nor Rain Nor MITM… The State of Email Security in 2015
  https://media.ccc.de/v/32c3-7255-neither_snow_nor_rain_nor_mitm_the_state_of_email_security_in_2015
- Let's Encrypt
  https://media.ccc.de/v/32c3-7528-let_s_encrypt_--_what_launching_a_free_ca_looks_like
- Netzpolitik in der Schweiz
  https://media.ccc.de/v/32c3-7205-netzpolitik_in_der_schweiz
- rad1o++
  Intro Talk: <https://media.ccc.de/v/camp2015-6884-the_rad1o>
  Updates: <https://media.ccc.de/v/32c3-7153-rad1o>
  HackRF! <http://greatscottgadgets.com/hackrf/>
- How the Great Firewall discovers hidden circumvention servers
  https://media.ccc.de/v/32c3-7196-how_the_great_firewall_discovers_hidden_circumvention_servers

Alle 32C3-Videos (mit Synchronisation und Untertitel):
https://media.ccc.de/c/32c3

Liebe Grüsse

Max & Beat